from __future__ import division
from keras.layers import Input, Conv2D, Lambda, merge, Flatten, MaxPooling2D, Dense
from keras.models import Model, Sequential
from keras.regularizers import l2
from keras import backend as K
from keras.optimizers import SGD, Adam
from keras.utils import plot_model
from keras.losses import binary_crossentropy
import tensorflow as tf
import numpy as np
from sklearn.preprocessing import binarize

# for the Hamming distance calculation
import operator
from itertools import imap

import numpy.random as rng
import os
import matplotlib.pyplot as plt
from sklearn.utils import shuffle

# initialize weights randomly
def W_init(shape, name=None):
    values = rng.normal(loc=0, scale=1e-2, size=shape)
    return K.variable(values, name=name)


# initialize biases randomly
def B_init(shape, name=None):
    values = rng.normal(loc=0.5, scale=1e-2, size=shape)
    return K.variable(values, name=name)


# building the model here - each half of the siamese net looks like this
input_shape = (64, 64, 1)
left_input = Input(input_shape)
right_input = Input(input_shape)

halfnet = Sequential()
halfnet.add(Conv2D(6, (9, 15), activation='sigmoid', input_shape=input_shape, kernel_initializer=W_init,
                   kernel_regularizer=l2(2e-4), padding="same"))
halfnet.add(Flatten())
# halfnet.add(Dense(4096, activation='sigmoid', kernel_regularizer=l2(1e-3), kernel_initializer=W_init, bias_initializer=B_init))

# encode each of the two inputs
encoded_left = halfnet(left_input)
encoded_right = halfnet(right_input)

# merge the two encoded inputs with HD between them
HD = lambda x: K.abs(tf.keras.backend.round(x[0]) - tf.keras.backend.round(x[1]))

both = merge([encoded_left, encoded_right], mode = HD, output_shape=lambda x: x[0])
prediction = Dense(1, activation='sigmoid', bias_initializer=B_init)(both)

# and create the final siamese network
siamese_net = Model(inputs=[left_input, right_input], outputs=prediction)

# then compile it
optimizer = Adam(0.00006)
siamese_net.compile(loss="binary_crossentropy", optimizer=optimizer)

print "Successfully compiled the model."

# count the parameters of our net
print "Parameters count:"
print siamese_net.count_params()
plot_model(halfnet, to_file='halfnet.png', show_shapes=True, show_layer_names=True)
plot_model(siamese_net, to_file='coldiris-siamese.png', show_layer_names=True, show_shapes=True)

print siamese_net.summary()