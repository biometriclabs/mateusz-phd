\chapter*{Appendix A: Harmonized Biometrics Vocabulary}
\label{chapter:background}

This Appendix aims at systematizing the biometrics-related terminology used in this Thesis. These terms are listed and defined below, with explanations provided from the ISO/IEC \emph{Information technology -- Vocabulary -- Part 37: Biometrics} standard \cite{HarmonizedVocabularyISO} with additional comments and extensions introduced by the Author where necessary.

\section{General concept terms}

\subsection{biometric characteristic}
\noindent
\textit{Biological and behavioral characteristic of an individual from which distinguishing, repeatable \textbf{biometric features} can be extracted for the purpose of \textbf{biometric recognition.}}

\subsection{biometric recognition}
\noindent
\textit{Automated recognition of individuals based on their biological and behavioral characteristics.}

\section{Terms for data in biometric systems}

\subsection{biometric sample}
\noindent
\textit{Analog or digital representation of \textbf{biometric characteristics} prior to \textbf{biometric feature extraction}.}

\noindent
In this work, \textbf{biometric samples} are equal to iris images. 

\subsection{biometric feature}
\noindent
\textit{Numbers or labels extracted from \textbf{biometric samples} and used for \textbf{comparison}.}

\noindent
In this work, \textbf{iris features} are understood rather as personal information that exists within the iris texture, than a mathematical representation of an iris extracted from an iris image. For the latter, the term \textbf{biometric template} is used.  

\subsection{biometric template}
\noindent
\textbf{Set of stored \textbf{biometric features} comparable directly to probe \textbf{biometric features}.}

\noindent
In this work, a \textbf{biometric template} is a mathematical representation of an iris extracted from an iris image by a particular iris processing method. 

\subsection{biometric data}
\noindent
\textit{\textbf{biometric sample} or aggregation of \textbf{biometric samples} at any stage of processing, e.g. biometric reference, biometric probe, \textbf{biometric feature}, or biometric property.}

\noindent
In this work, \textbf{biometric data} usually refers to an aggregation of biometric samples, in this case -- iris images.


\subsection{comparison decision}
\noindent
\textit{Determination of whether the \textbf{biometric probe} and \textbf{biometric reference} have the same \textbf{biometric} source, based on \textbf{comparison score(s)}, a decision policy including a \textbf{threshold}, and possibly other inputs. A \textbf{match} is a positive comparison decisions, whereas a \textbf{non-match} is a negative one.}

\noindent
In this work, where applicable, the \textbf{comparison decision} is issued based on a \textbf{comparison score} between two \textbf{biometric templates} and an appropriate \textbf{threshold}.

\subsection{threshold}
\noindent
\textit{Numerical value (or set of values) at which a decision boundary exists.}

\subsection{comparison score}
\noindent
\textit{Numerical value (or set of values) resulting from a \textbf{comparison}.}

\subsection{dissimilarity score, distance score}
\noindent
\textit{\textbf{Comparison score} that decreases with similarity.}

\noindent
In this work, three iris recognition methods used by the Author yield dissimilarity scores: in the form of a fractional Hamming distance (OSIRIS, MIRLIN), or in the form of a proprietary metric (IriCore).

\subsection{similarity score}
\noindent
\textit{\textbf{Comparison score} that increases with similarity.}

\noindent
In this work, the only iris recognition method used by the Author yields a similarity score in the form of a proprietary metric (VeriEye).

\section{Functioning terms}

\subsection{biometric enrolment}
\noindent
\textit{Act of creating and storing a biometric enrolment data record in accordance with an enrolment policy.}

\noindent
In this work, this term is understood as a process of creating the \textbf{biometric template} (a numerical representation) from a \textbf{biometric sample} (an iris image) by performing \textbf{biometric feature extraction}.

\subsection{biometric feature extraction}
\noindent
\textit{Process applied to \textbf{biometric sample} with the intent of isolating and outputting repeatable and distinctive numbers or labels which can be compared to those extracted from other \textbf{biometric samples}.}

\subsection{comparison}
\noindent
\textit{Estimation, calculation, or measurement of similarity or dissimilarity between biometric probe(s) and biometric reference(s).}

\noindent
In this work, a comparison is usually performed between two \textbf{biometric templates}, since we do not necessarily divide the templates into probe and reference. 

\section{Interacting terms}

\subsection{biometric presentation attack}
\noindent
\textit{Presentation to the biometric capture subsystem with the goal of interfering with the operation of the biometric system.}

\noindent
In this work, a presentation attack usually refers to the presentation of a cadaver eye to the iris recognition camera.


\section{Applications terms}


\subsection{biometric identification}
\noindent
\textit{Process of searching against a \textbf{biometric enrolment database} to find and return the \textbf{biometric reference identifier(s)} attributable to a single individual.}


\subsection{biometric verification}
\noindent
\textit{Process of confirming a biometric claim through \textbf{biometric comparison}.}

\section{Performance terms}

\subsection{failure to enrol, FTE}
\noindent
\textit{Failure to create and store a \textbf{biometric enrolment data record}.}

\noindent
In this work, \textbf{failure to enrol} refers to the inability to create a \textbf{biometric template} by a given iris recognition method.

\subsection{failure-to-enrol rate, FTE rate, FTER}
\noindent
\textit{Proportion of a specified set of \textbf{biometric enrolment} transactions that resulted in a \textbf{failure to enrol}.}

\subsection{false match}
\noindent
\textit{\textbf{Comparison decision} of \textbf{match} for a biometric probe and a biometric reference that are from different biometric capture subjects.}

\noindent
In this work, this means a \textbf{match} decision for two \textbf{biometric templates} that are from different irises. Also: a \textbf{false positive}.

\subsection{false match rate, FMR}
\noindent
\textit{Proportion of the completed biometric non-mated comparison trials that result in a \textbf{false match}.}

\subsection{false non-match}
\noindent
\textit{\textbf{Comparison decision} of \textbf{non-match} for a biometric probe and biometric reference that are from the same biometric capture subject and of the same biometric characteristic.}

\noindent
In this work, this means a \textbf{non-match} decision for two \textbf{biometric templates} that are from the same iris. Also: a \textbf{false negative}.

\subsection{false non-match rate, FNMR}
\noindent
\textit{Proportion of the completed biometric mated comparison trials that result in a \textbf{false non-match}.}
