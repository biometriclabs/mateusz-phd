\chapter{Iris representation with iris-driven image filtering}
\label{chapter:filters}

\section{Introduction}
As shown in the previous Chapter, applying deep-learning based iris image segmentation methodology, coupled with existing iris representation and matching pipeline of the OSIRIS software, allows for a significant improvement in recognition accuracy for challenging post-mortem samples, with recognition errors decreased not only beyond those offered by the original OSIRIS, but also better than the performance of the commercial, state-of-the-art methodologies -- IriCore.

However, despite the progress that has been made, the accuracy still remains far from perfect for capture horizons longer than 48 hours post-mortem. Since the image segmentation stage is already being executed without major errors for almost all of the samples, achieving even higher recognition rates requires a novel approach to iris encoding.    
 
Widely used 	in iris recognition, as introduced by Daugman more than 25 years ago, is the quantization of filter response over the iris image, most commonly using Gabor wavelets \cite{DaugmanArticle} or their approximations, as used in the OSIRIS algorithm \cite{OSIRIS}. To date, several alternatives were proposed, such as LoG pyramids \cite{Wildes1996}, Haar wavelets \cite{KimPatentIriTech}, or Zak-Gabor wavelet packets \cite{CzajkaPacut}. A review of existing approaches to iris representation is delivered in Sec. \ref{sec:introduction:irisrecognitionmethods}. 
    
Despite recent advancements in field of deep-learning models for iris discrimination, these 'traditional' methods offer the interpretability, as opposed to the predictions provided by a DCNN-based algorithm. On the other hand, a data-driven model can offer performance that is superior to a hand-crafted model because of its ability to learn the most efficient way to represent the data from the data itself.  
    
We make an attempt at improving the efficiency of the iris representation by introducing data-driven filters that are learnt from post-mortem iris images. A shallow -- \ie with one convolutional layer -- Siamese network is employed for learning the novel iris descriptor in a form of two dimensional filter kernels that can be further used in any conventional (\ie Daugman's) iris recognition pipeline. Such set of filters is then used to optimize the original OSIRIS filter bank to improve the method's accuracy for post-mortem iris images. 

\section{Related work}

\subsection{One-shot recognition and Siamese networks}
Recent advancements in deep learning allowed DCNN-based image classifiers to achieve performance superior to any other class of methods. However, their one important drawback is the need for large quantities of labeled data for supervised learning. This becomes a problem in applications where a prediction must be obtained about the data belonging to a class that the model had never seen during the training.   

Siamese networks, on the other hand, perform well in the so called \emph{one-shot} recognition tasks, being able to give reliable similarity prediction about the samples from classes that were not included in the model training phase. Koch \etal \cite{koch2015siamese} introduces a deep convolutional model architecture consisting of two convolutional branches sharing weights and joined by a merge layer with $L_1$ cost function describing distance between the two inputs $x_1,x_2$ :
\begin{equation}
\label{equation:l1}
	L_1(x_1,x_2) = |f(x_1) - f(x_2)|
\end{equation}

where $f$ denotes the encoding function. This is combined with a sigmoid activation of the single neuron in the last layer, which maps the output to the range of [0, 1]. This architecture is trained and tested in a class-disjoint manner on the Omniglot dataset, which comprises handwritten characters belonging to 50 different alphabets, achieving accuracy of 92\%, which is almost on par with human performance. 

The applications of siamese networks include many areas, most importantly one-shot image recognition, with good benchmark performances achieved on well-known datasets such as Omniglot (written characters recognition for multiple alphabets, 95\% accuracy) and ImageNet (natural image recognition with 20000 classes, 87.8\% accuracy) \cite{VinyalsSiameseNIPS2016}, but also object co-segmentation \cite{SiameseObjectCoSegmentation2018}, object tracking in video scenes \cite{bertinetto2016fullysiamesetracking}, signature verification \cite{BromleySiamense1993}, and even matching resumes to job offers \cite{MaheshwarySiameseResumes2018}. 
 
\subsection{Data-driven image descriptors}
Several approaches to learning feature descriptors for image matching have been explored, mostly in the field of visual geometry and mapping for image stitching, orientation detection, and similar general-purpose approaches.

Simo-Serra \etal \cite{DeepDescSimoSerra2015} present a novel point descriptor, whose discriminative feature descriptors are learnt from the real-world, large datasets of corresponding and non-corresponding image patches from the MVS dataset, containing image patches sampled from 3D reconstructions of the Statue of Liberty, Notre Dame cathedral, and Half Dome in Yosemite. The approach is reported to outperform SIFT, while being able to serve as a drop-in replacement for it. The method employs a Siamese architecture of two coupled CNNs with three convolutional layers each, whose outputs are patch descriptors, and an $L_2$ norm of the output difference is minimized between positive patches and maximized otherwise.

A similar approach is  demonstrated by Zagoruyko and Komodakis \cite{ZagoruykoK15SiamesePatches}, who train a similarity function for comparing image patches directly from the data employing several methods, one of them being a Siamese model with two CNN branches sharing weights, connected at the top by two fully connected layers. 

Yi \etal introduce a method that is intended to serve as a full SIFT replacement, not only as a drop-in descriptor replacement \cite{LIFT}. The deep-learnt approach consists of a full pipeline with keypoint detection, orientation estimation, and feature description, trained in a form of a Siamese quadruple network with two positive (corresponding) input patches, one negative (non-corresponding) patch, and one patch without any keypoints in it. Hard mining of difficult keypoint pairs is employed, similarly to \cite{DeepDescSimoSerra2015}.

DeTone \etal \cite{SuperPointDeTone2018} introduce a so-called SuperPoint network and a framework for self-supervised training of interest point detectors and descriptors that are able to operate on the full image as an input, instead of image patches. The method is able to compute both interest points and their descriptors in a single network pass.

Moving to the field of biometrics, Czajka \etal \cite{CzajkaBSIFIrisDesc2019} have recently employed human-inspired, iris-specific binarized statistical image features (BSIF) filters from iris image patches derived from an eye-tracking experiment, during which human iris examiners were asked to classify iris pairs \cite{CzajkaBSIFIrisDesc2019}. Data-driven BSIF filters were also studied by Bartuzi \etal for the purpose of person recognition based on thermal hand representations \cite{EwelinkaHandIWBF2018}.  

\section{Proposed methodology}

\subsection{Preprocessing of the training data}
To train our new, post-mortem-aware iris feature descriptor, we use NIR iris images from the Warsaw-BioBase-Postmortem-Iris-v1.1 and Warsaw-BioBase-Postmortem-Iris-v2, which were then processed with the best performing segmentation model from the previous experiment ({\it \textbf{fine v4}}) and normalized using the algorithm introduced in Sec. \ref{sec:HoughMT} to come up with polar iris images $512\times64$ pixels in size. 

The first step in preparing the training data is to ensure that the polar iris images are aligned within any given class, \ie that there are no vertical shifts in the images that reflect eyeball rotation in the cartesian coordinate system. Since the acquisition process could not be controlled, such rotations are present in the dataset, as shown in Fig. \ref{fig:code_shifting}. To reduce the resulting shift in the polar iris images, we performed the image alignment procedure, which involved manual annotations of the eye corners. This allowed to calculate a relative rotation of the eyeball represented in the two images, and in turn the amount of pixels, by which the polar image must be shifted, see Fig. \ref{fig:code_shifting}. Typical iris recognition methods shift iris codes in the matching stage to compensate for eyeball rotation, instead of shifting images, including the OSIRIS used in our experiments. Therefore there is no justification to make the neural network learn how to discriminate between irises that are not ideally spatially aligned.

\begin{figure}[t]
\centering
\includegraphics[width=1\linewidth]{Filters/code_shifting}
\caption{The procedure for aligning the polar-coordinate polar iris images based on eye corner location to compensate rotation of the camera during image acquisition.}
\label{fig:code_shifting}
\end{figure}

The resulting aligned polar iris images were then subject to examination in respect to the amount of occlusions caused by eyelids or eyelashes. During iris verification a binary occlusion mask is usually employed to discard regions with noise present. On the other hand, introducing noisy data during the training phase could bias the model into considering noise (here: eyelids) as iris features, which is not desired. To ensure good quality of the training data, we divide each polar iris image into two \emph{patches}. Since in our data eyelid occlusions are only present either on the left or on the right portion of the polar iris image, this enables discarding such samples while at the same time saving the other, unaffected patch, Fig. \ref{fig:patch_creation}. 1801 patches in total were extracted for training.  

\begin{figure}[h!]
\centering
\includegraphics[width=1\linewidth]{Filters/patch_creation}
\caption{Procedure for iris patch creation.}
\label{fig:patch_creation}
\end{figure}

\subsection{Model architecture and filter learning}
\begin{figure}[h!]
\centering
\includegraphics[width=\linewidth]{Filters/siamese_net}
\caption{Siamese network used for iris feature representation learning.}
\label{fig:siamese_net}
\end{figure}

For learning the iris-specific filters a shallow Siamese architecture is used, Fig. \ref{fig:siamese_net}, composed of two branches, each responsible for encoding of one image from the image pair being compared, comprising a single convolutional layer with 6 kernels of size $9\times15$, to reflect the number of filters found in the OSIRIS, and the size of the smallest OSIRIS filters. The initial calculations revealed that $9\times15$ kernels allows better results than $9\times27$ or $9\times51$ (also found in OSIRIS), hence the decision. The weights are shared between the two branches. Following the convolutional layers is a \emph{merge} layer calculating the $L_1$ distance (see Eq. \ref{equation:l1}), or the absolute mean, between the two sets of features from the convolutional layer. A single neuron with a sigmoid activation function is then applied to yield a prediction from the range [0, 1], with 0 being a perfect match between the two images, and 1 being a perfect non-match.     

The training data is passed to the network in batches containing 32 pairs of iris patches, out of which 16 are genuine, and 16 are impostor pairs, randomly sampled without replacement from the dataset during each training iteration, with a total of 20000 iterations. ADAM optimizer -- a first-order gradient-based optimization method, based on adaptive estimates of lower-order moments: an exponentially decaying average of past gradients and squared past gradients \cite{ADAM}, is used with $lr=0.0006$.

\subsection{Feature selection}
\begin{figure}[t!]
\centering
\hskip-1mm {\bf Filter kernels:} \hskip34mm {\bf Example iris codes:}\\
\includegraphics[width=0.34\linewidth]{Filters/siamese_filters}\hskip5mm
\includegraphics[width=0.44\linewidth]{Filters/siamese_codes}\\\vskip3mm
{\bf Mean iris code value distributions:}\\
\includegraphics[width=\linewidth]{Filters/wavelet_distributions}
\caption{Learnt kernels from the Siamese network, an example set of iris codes they produce, and distributions of mean code values. Kernel 6 is discarded.}
\label{fig:siamese_filtersAndCodes}
\end{figure}

The learnt filter kernels, together with example iris codes that they produce, as well as distributions of mean iris code values produced by each of them are illustrated in Fig. \ref{fig:siamese_filtersAndCodes}. By analyzing the distributions of mean iris code values obtained by each of the new filters, we see that codes produced by the sixth filter do not represent the iris well, as most of the texture information is lost during encoding, resulting in a mostly zeroed iris code. This filter is discarded from all further experiments.
 
Notably, employing only the iris-specific filter kernels instead of those found in OSIRIS did not yield better results -- perhaps due to the fact that regular iris texture is well represented using the conventional Gabor wavelets, and the newly learnt filters are necessary to boost the performance for difficult, decay-affected samples. To utilize these new filters, and to offer an advantage over the baseline method, a modification of the OSIRIS Gabor filter bank was performed to propose a hybrid filter bank comprising a combination of Gabor wavelets and the iris-specific kernels.

This procedure employed by Sequential Feature Selection, with a combination of Sequential Forward Selection (SFS) and Sequential Backward Selection (SBS), which involve adding the most discriminant features to the classifier, while removing the least discriminatory ones. During the feature selection procedure, post-mortem-specific filters were added to the original OSIRIS filter bank, whereas  those OSIRIS filters, which do not contribute to decreasing the error rates were removed. This estimation is performed on the Warsaw-BioBase-Postmortem-Iris-v3, which is subject-disjoint with the databases used for network training. The error metric minimized during the feature selection is the EER obtained for samples acquired up to 60 hours post-mortem, in the same way as done earlier in Sec. \ref{sec:matching:experiments}.\\

The feature selection procedure can be enclosed in the following steps, starting with the original, unmodified OSIRIS filter bank comprising six Gabor wavelets:
\begin{itemize}
	\item [] {\bf Step 1.} Calculate the performance obtained using the current filter bank and each of the siamese filters added independently.
	\item [] {\bf Step 2.} \textcolor{mygreen}{\bf Perform SFS by adding the most contributing filter to the filter bank.}
	\item [] {\bf Step 3.} Calculate the performance obtained using the filter bank obtained in the previous step with each of the OSIRIS filters removed independently.
	\item [] {\bf Step 4.} \textcolor{myred}{\bf Perform SBS by removing the least contributing filter from the filter bank.}
	\item [] {\bf Step 5.} $\rightarrow$ Go back to Step 1 and repeat until the error metric stops improving.
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=\linewidth]{Filters/feature_selection}
\caption{Illustration of the filter selection for the new filter bank using Sequential Forward Selection and Sequential Backward Selection. Iris codes for an example iris produced by the proposed hybrid filter bank at different stages of filter selection.}
\label{fig:siamese_featureSelection}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=0.8\linewidth]{Filters/sfs_eers}
\caption{SFS/SBS filter selection: Equal Error Rates in subsequent iterations of the procedure are shown. The filter selection is stopped at SBS iteration 2.}
\label{fig:SFS}
\end{figure}


After the SFS/SBS feature selection procedure involving four iterations of SFS and SBS, Fig. \ref{fig:siamese_featureSelection}, the EER was decreased by almost a third, from 6.40\% obtained for the 60 hours post-mortem time horizon for the original OSIRIS filter bank, to 4.39\% obtained for the new, hybrid filter bank, Fig. \ref{fig:SFS}. Although performing an additional feature selection iteration does give a small decrease in EER, we observed an increase in EER for capture time horizons larger than 60 hours, and adding yet another iteration ({\it SBS iteration 3} in Fig. \ref{fig:SFS}) significantly increases the error.

This filter bank is then used in the final testing of our iris recognition pipeline in the following section. The final set of filter kernels is shown in Fig. \ref{fig:siamese_featureSelection}.


\section{Results and discussion}

\subsection{Testing data and methodology}
For testing, the same database is used as in Chapter \ref{chapter:segmentation}, namely the Warsaw-BioBase-Postmortem-Iris-v3. This way, the results obtained here are directly comparable to those obtained during the evaluation of the proposed segmentation method. During testing, the same protocol as described in Sec. \ref{sec:matching:experiments} applies, we generate all possible genuine and impostor scores between images that were captured up to a certain time horizon. 

\subsection{Recognition accuracy}
Figures \ref{fig:ROCs:cold_short_newfilters}-\ref{fig:ROCs:cold_long_newfilters} present ROC curves obtained using the newly introduced filter bank, compared against ROCs corresponding to the best results obtained when only the segmentation stage is replaced with the proposed modifications.

When analyzing graphs presented in Fig. \ref{fig:ROCs:cold_short_newfilters}, which correspond to samples collected up to 12, 24, and 48 hours post-mortem, we do not see improvements in recognition performance measured by the EER, that is EER=0.56\%$\rightarrow$0.76\%, 0.69\%$\rightarrow$0.68\%, and 2.45\%$\rightarrow$2.57\%, respectively. However, the shapes of the red graphs corresponding to the scores obtained with the new filter bank show an improvement over the black graphs in the low FMR registers, meaning that the proposed system offers higher recognition rates in situations, when very few false matches are allowed.

Moving to more distant post-mortem sample capture time horizons, Fig. \ref{fig:ROCs:cold_mid_newfilters}, the advantage of the proposed method becomes clearly visible in both the decreased EER values, as well as in the shapes of the ROC curves. Applying domain-specific filters allowed to reduce EER from 6.40\% to 4.39\%, from 8.12\% to 5.86\%, and from 9.99\% to 7.78\%, for samples acquired less than 60, 72, and 110 hours post-mortem, respectively. 

Finally, Fig. \ref{fig:ROCs:cold_long_newfilters} presents ROC curves obtained for samples collected during the three longest subject observation time horizons, namely up to 160, 210, and 369 hours after death. Here as well, a visible improvement offered by the new feature representation scheme is reflected in the decreased EER values -- 14.59\%$\rightarrow$11.88\% for samples collected up to 160 hours, 17.09\%$\rightarrow$14.98\% for those captured up to 210 hours, and 21.36\%$\rightarrow$19.27\% for the longest and most difficult set, encompassing images acquired up to 369 hours (more than 15 days).

\begin{figure}[h!]
\centering
\includegraphics[width=0.49\linewidth]{Filters/ROCs_postmortem_new_12}\hskip1mm
\includegraphics[width=0.5\linewidth]{Filters/ROCs_postmortem_new_12_closeup}\\\vskip2mm
\includegraphics[width=0.49\linewidth]{Filters/ROCs_postmortem_new_24}\hskip1mm
\includegraphics[width=0.5\linewidth]{Filters/ROCs_postmortem_new_24_closeup}\\\vskip2mm
\includegraphics[width=0.49\linewidth]{Filters/ROCs_postmortem_new_48}\hskip1mm
\includegraphics[width=0.5\linewidth]{Filters/ROCs_postmortem_new_48_closeup}
\caption{Receiver Operating Characteristic curves obtained when comparing post-mortem samples with different observation time horizons: 12, 24, and 48 hours post-mortem, plotted for two baseline iris recognition methods OSIRIS (blue) and IriCore (green), OSIRIS with new segmentation introduced in Chapter \ref{chapter:segmentation} (black), as well as OSIRIS with both the improved segmentation and new filter set (red). Plots on the right are close-ups of the ones on the left in each pair.}
\label{fig:ROCs:cold_short_newfilters}
\end{figure}


\begin{figure}[h!]
\centering
\includegraphics[width=0.49\linewidth]{Filters/ROCs_postmortem_new_60}\hskip1mm
\includegraphics[width=0.5\linewidth]{Filters/ROCs_postmortem_new_60_closeup}\\\vskip2mm
\includegraphics[width=0.49\linewidth]{Filters/ROCs_postmortem_new_72}\hskip1mm
\includegraphics[width=0.5\linewidth]{Filters/ROCs_postmortem_new_72_closeup}\\\vskip2mm
\includegraphics[width=0.49\linewidth]{Filters/ROCs_postmortem_new_110}\hskip1mm
\includegraphics[width=0.5\linewidth]{Filters/ROCs_postmortem_new_110_closeup}
\caption{Same as in Fig. \ref{fig:ROCs:cold_short_newfilters}, but plotted for comparison scores obtained from samples collected up to 60, 72, and 110 hours post-mortem.}
\label{fig:ROCs:cold_mid_newfilters}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=0.49\linewidth]{Filters/ROCs_postmortem_new_160}\hskip1mm
\includegraphics[width=0.5\linewidth]{Filters/ROCs_postmortem_new_160_closeup}\\\vskip2mm
\includegraphics[width=0.49\linewidth]{Filters/ROCs_postmortem_new_210}\hskip1mm
\includegraphics[width=0.5\linewidth]{Filters/ROCs_postmortem_new_210_closeup}\\\vskip2mm
\includegraphics[width=0.49\linewidth]{Filters/ROCs_postmortem_new_370}\hskip1mm
\includegraphics[width=0.5\linewidth]{Filters/ROCs_postmortem_new_370_closeup}
\caption{Same as in Fig. \ref{fig:ROCs:cold_short_newfilters}, but plotted for comparison scores obtained from samples collected up to 160, 210, and 369 hours post-mortem.}
\label{fig:ROCs:cold_long_newfilters}
\end{figure}

\subsection{False Non-Match Rate dynamics}
In addition to the ROCs, we have also calculated the False Non-Match Rate (FNMR) values at acceptance thresholds which allow the False Match Rate (FMR) values to stay below certain values, namely 0.1\%, 1\% and 5\%, Figs. \ref{fig:FNMR01_new}, \ref{fig:FNMR1_new}, \ref{fig:FNMR5_new}, respectively. This is done to reveal the dynamics of the FNMR as a function of post-mortem sample capture horizon, and therefore to know the chances for a false non-match as time since death progresses. We plot this dynamics for the two baseline methods: original OSIRIS and IriCore, as well as for the best DCNN-based segmentation modification -- the {\it \textbf{fine v4}} model, and the proposed iris representation, again coupled with the {\it \textbf{fine v4}} segmentation.

\begin{figure}[htb]
\centering
\includegraphics[width=\linewidth]{Filters/FNMRs01_new}
\caption{Dynamics of False Non-Match Rates (FNMR) in the function of post-mortem sample capture horizon for a set False Match Rate (FMR) of 0.1\%, plotted for OSIRIS (blue) and IriCore (red), OSIRIS with proposed segmentation (yellow), and OSIRIS with both the proposed segmentation and the new filter set (violet).}
\label{fig:FNMR01_new}
\end{figure}

While acceptance thresholds allowing FMR of 5\% or even 1\% can be considered as very relaxed for large-scale iris recognition systems, such criteria make sense in a forensic scenario. In such, the goal of the automatic system is typically to aid a human expert by proposing a candidate list, while minimizing the chances of missing the correct hit. Therefore, allowing a higher False Match Rate will make it more likely for the correct hit to appear within the candidate list. 

The proposed modification to the iris feature representation is again able to outperform the traditional OSIRIS filter bank consisting of only Gabor kernels during each moment of the increasing sample acquisition time horizon for the two most restrictive FMR thresholds of 0.1\% and 1\%, Figs. \ref{fig:FNMR01_new} and \ref{fig:FNMR1_new}, and is only slightly worse for the first three shortest time horizons up to 48 hours, when FMR of 5\% is allowed, Fig. \ref{fig:FNMR5_new}.

Notably, for each moment during the increasing post-mortem sample capture time horizon, our proposed approach consistently offers an advantage over the other two algorithms, allowing to reach nearly perfect recognition accuracy for samples collected up to a day after a subject's death. This difference in favor of our proposed solution is even larger when the acceptance threshold is relaxed to allow for 5\% False Match Rate - in such scenario, we can expect no false non-matches in the first 24 hours, and only approx. 1.5\% chance of a false non-match in the first 48 hours. The new method for iris feature representation, on the other hand, shows its greatest advantage in the acquisition horizons that are longer than 48 hours, being able to reduce the errors by as much as a third in the 60-hour and 72-hour capture moments.  

\begin{figure}[h!]
\centering
\includegraphics[width=\linewidth]{Filters/FNMRs1_new}
\caption{Similar to Fig. \ref{fig:FNMR1_new}, but for a set FMR=1\%.}
\label{fig:FNMR1_new}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=\linewidth]{Filters/FNMRs5_new}
\caption{Similar to Fig. \ref{fig:FNMR1_new}, but for a set FMR=5\%.}
\label{fig:FNMR5_new}
\end{figure}

\section{Conclusions}
In this Chapter we have introduced the novel iris feature representation method, employing iris-specific image filters that are learnt directly from the data, and are thus optimized to be resilient against post-mortem changes affecting the eye during the increasing sample capture time since death. By finding the optimal combination of typical Gabor-wavelets-based iris encoding with the new post-mortem aware encoding we are able to further reduce the recognition errors by as much as one third, {\bf proving the third thesis formulated in Sec. \ref{sec:theses}, namely that the proposed post-mortem-specific iris feature representation further improves recognition accuracy for cadaver samples.} 

  


