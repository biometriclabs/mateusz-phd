\chapter{Presentation attack detection for cadaver iris}
\label{chapter:PAD}

\section{Is that eyeball dead or alive?}
\label{sec:PAD:intro}
With increasing importance that biometric authentication gains in our daily lives, fears fueled by Hollywood's depictions of severed thumbs and plucked eyeballs are increasingly common among users, regarding the possibility of unauthorized access to one's data, identity, or assets after demise. Law enforcement officers in the U.S. have been already using the fingerprints of the deceased suspects to unlock their iPhones \cite{ColdTouchID}, which immediately brings up the topic of whether liveness detection should be implemented in such devices. With a constantly growing market share of iris recognition, and recent research, including results presented in this Thesis, proving that iris biometrics in a post-mortem scenario can be viable \cite{TrokielewiczPostMortemICB2016,TrokielewiczPostMortemBTAS2016,BolmeBTAS2016,TrokielewiczTIFS2018}, these concerns may soon become a reality for iris as well. In a recent interview for the {\it IEEE Spectrum} online magazine, Czajka discussed the issue of liveness detection, which is crucial in cases when \emph{we don't want} our biometric traits to be used after death \cite{ColdIrisSPECTRUM}. 

To the Author's best knowledge, there are no prior papers or published research regarding the topic of discerning live irises from dead ones. This Chapter presents a method for iris liveness detection in a post-mortem setting, using a static iris image and based on a deep convolutional neural network fine-tuned with a dataset of post-mortem and live iris images.

\section{Related work}
\label{sec:PAD:related}
\subsection{Presentation Attack Detection in iris recognition}
Presentation attack detection is already a well established research area in the field of biometrics. Existing methods include detection of fake representations of irises (paper printouts, textured contact lenses, prosthetic eyes, displays), or a non-conformant use of an actual eye. The most popular techniques used in iris PAD use various image texture descriptors (Binarized Statistical Image Features (BSIF) \cite{Komulainen_IJCB_2014}, Local Binary Patterns (LBP) \cite{Doyle_ICB_2013}, Binary Gabor Patterns (BGP) \cite{Lovish_CAIP_2015}, Local Contrast-Phase Descriptor (LCPD) \cite{Gragnaniello_TIFS_2015}, Local Phase Quantization (LPQ) \cite{Sequeira_TSP_2016}, Scale Invariant Descriptor (SID) \cite{Gragnaniello_SITIS_2014}, Scale Invariant Feature Transform (SIFT) and DAISY \cite{Pala_CVPR_2017}, Weber Local Descriptor (WLD) \cite{Gragnaniello_TIFS_2015}, or Wavelet Packet Transform (WPT) \cite{Chen_PRL_2012}), image quality descriptors \cite{Galbally_Handbook_2016}, or deep-learning-based techniques \cite{Menotti:TIFS:2015,He_BTAS_2016,Pala_CVPR_2017,Raghavendra_WACV_2017}. If hardware adaptations are possible one may consider multi-spectral analysis \cite{Thavalengal_TCE_2016} or estimation of three-dimensional iris features \cite{Pacut_ICCST_2006,Hughes_HICSS_2013} for PAD. Making the PAD more complex, one may consider measuring micro-movements of an eyeball, either using Eulerian video magnification \cite{Raja_TIFS_2015} or by using an eye-tracking device \cite{Rigas_PRL_2015}, or measuring  pupil dynamics \cite{Czajka_TIFS_2015}. An extensive review of the state of the art in PAD for iris recognition, including a systematization of attack methodologies and countermeasures, can be found in \cite{CzajkaPADSurvey2018}. Independent evaluations of algorithms detecting iris paper printouts and textured contact lenses can be studied from the LivDet-Iris competition series\footnote{\url{http://livdet.org/}}, which has had its last edition in 2017 \cite{Yambay2017}.

Despite the abundance of proposed methods, there are still no published papers that would explore the concept of presentation attack detection in a scenario when cadaver eyes are used to perform a presentation attack. However, one can still envisage such situation, in which a dead eye is used with an unsupervised biometric system to gain an unauthorized access.

\subsection{A feature-learning approach}
We have already shown that although post-mortem iris recognition is, to some extent, possible with current software, it also poses challenges that we do not yet have solutions to \cite{TrokielewiczPostMortemBTAS2016}. Most importantly, there currently are no mathematical models that would explain the iris' behavior over the course of post-mortem time horizon, \ie quantify and predict the changes that the iris may undergo after one's demise. Therefore, when aiming at discerning live irises from dead ones, a potentially promising way of solving this problem is to rely on the feature-learnt approach that utilizes post-mortem and live iris images to develop a model directly from the data.       

\subsection{Visual explanations from deep networks}
\label{sec:CAM}
DCNNs in their basic designs do not provide a human-interpretable explanation for their decisions. This makes such methods badly suited as a tool for assisting human experts in a courtroom scenario, for instance, because a softmax layer output cannot be expected to convince the jury of a person's innocence or guilt.

To alleviate these issues, several techniques have recently been proposed, including class activation mapping (CAM), first introduced by Zhou \etal \cite{CAMZhou} for identification of discriminative image regions, decisive for the model prediction. The authors achieve this by removing fully-connected layers in the popular network architectures (AlexNet, GoogLeNet, VGG), and replacing them with global average pooling layers followed only by a softmax layer. As a result, image regions that are important for discrimination are highlighted in a heatmap-like manner. Selvaraju \etal introduce improvement over the Zhou's method with Grad-CAM \cite{GradCAMSelvaraju}, which does not require any changes to the network's architecture, making it easier to use and more flexible. The solution yields coarse localization heat-maps highlighting the regions that contribute the most to the model's prediction, but also high-resolution visualization of features learned by the network, obtained from guided back-propagation introduced by Springenberg \etal in \cite{GuidedBackPropSpringenberg}. By combining these two, the authors obtain fine-grained importance maps, which apart from highlighting a coarse \textit{region} of the image that is considered discriminatory, also allow insight into which \textit{features} are important. An example visualization of these techniques is shown in Fig. \ref{fig:gradcam:eel}.


\begin{figure}[t]
	\centering
	\begin{subfigure}[!htb]{0.24\textwidth}
	\centering
		\includegraphics[width=0.95\textwidth]{Recognition/eel}
        \caption{Original eel image}
    	\end{subfigure}\hskip1mm
	\begin{subfigure}[!htb]{0.24\textwidth}
	\centering
		\includegraphics[width=0.95\textwidth]{Recognition/eel-gradcam}
        \caption{CAM output}
    	\end{subfigure}\hskip1mm
    		\begin{subfigure}[!htb]{0.24\textwidth}
	\centering
		\includegraphics[width=0.95\textwidth]{Recognition/eel-guided_backprop}
        \caption{backpropagation}
    	\end{subfigure}\hskip1mm
    		\begin{subfigure}[!htb]{0.24\textwidth}
	\centering
		\includegraphics[width=0.95\textwidth]{Recognition/eel-guided_gradcam}
        \caption{Grad-CAM output}
    	\end{subfigure}\hskip1mm
	\caption{What makes eel an eel, according to a DCNN? Illustration of the CAM \cite{CAMZhou} and Grad-CAM \cite{GradCAMSelvaraju} techniques for assessing important, discriminatory regions during image processing by a CNN.}
\label{fig:gradcam:eel}
\end{figure}

These methods can be important for building a robust liveness detection for two reasons, both of them being explored in this Chapter. First, class activation mapping can help analyze the potential bias in the raw data that interfere with the network training, causing the model to learn features that are not directly related to the task. For instance, learning the presence of metal retractors used to open cadaver eyes, and missing in live eyes, ends up with perfect accuracy albeit with no relation to PAD accuracy. Second, we hope to gain some knowledge regarding the iris/eye features being employed by the network for discriminating between live and dead irises.

\section{Experimental dataset}
\label{sec:PAD:data}
For the purpose of this study, we used the Warsaw-BioBase-PostMortem-Iris-v1 dataset, which gathers 1,330 iris images collected from 17 cadavers.  

Since this database does not offer any ante-mortem samples, we had to collect a complementary dataset of iris images collected from live people. To mimic the original acquisition protocol as closely as possible, and thus to minimize a possible bias in training the DCNN, we have employed the same iris camera as was used in the post-mortem acquisition -- IriShield M2120U. Example iris images used for the development of the PAD are shown in Fig. \ref{fig:samplesFromDataset}.


\section{Proposed methodology and evaluation}
\label{sec:PAD:methodology}

\subsection{Model architecture}
For our solution, we employed the VGG-16 model pre-trained on natural images from the ImageNet database \cite{VGGSimonyanCNNsForRecognition2014}, which has been shown to repeatedly achieve excellent results in various classification tasks after minor adaptation and re-training. We performed a modification to the last three layers of the original network to fit the binary classification into \emph{live} and \emph{post-mortem} types of images, and fine-tuned the original network weights with our dataset of iris images representing live and post-mortem classes.

\subsection{Training and evaluation procedure}
For the network training and testing procedure, 20 subject-disjoint train/test data splits were created by randomly assigning the data from 3 subjects to the test subset, and the data from the remaining subjects to the train subset, both for the \emph{live} and \emph{post-mortem} parts of the database. These twenty splits were made with replacement, making them statistically independent. The network was then trained with each train subset independently for each split, and evaluated on the corresponding test subset. This procedure gives 20 statistically independent evaluations and allows to assess the variance of the estimated error rates. The training encompassing 10 epochs was performed with stochastic gradient descent as the minimization method with momentum $m=0.9$ and learning rate of 0.0001, with the data being passed through the network in mini batches of 16 images.

During testing, a prediction of the \emph{live} or \emph{post-mortem} class-wise probability was obtained from the softmax layer, together with a corresponding predicted categorical label. The probabilities of \emph{post-mortem} samples belonging to their class are also associated with a time that elapsed since death until the sample acquisition. This allows for the analysis of classification accuracy in respect to the \emph{post-mortem} time horizon. 

\section{Assessing the bias in post-mortem and live samples}
\label{sec:PAD:bias}
We are aware that despite our efforts to keep the acquisition protocols as alike as possible, there will be some bias in the data. Dataset bias has been blamed for limiting the progress and scope of object recognition research due to problematic generalization of deep-learnt methods from one dataset to another \cite{BIAS}. The aim of this Section is thus to carefully examine these differences, discuss their importance and impact on the experiments, and to propose countermeasures, where possible. For this purpose, we employ two types of bias evaluations - a qualitative one, employing class activation maps (cf. Sec. \ref{sec:CAM}), and a quantitative one, utilizing ISO-recommended iris image quality metrics.

\subsection{Qualitative bias assessment}
There are differences between the samples that originate from different presentations of the biometric characteristics in live and post-mortem scenarios. This is most notably related to the appearance of eyelids, which in the post-mortem data are often pulled apart with a metal retracting device to keep the eye open for image acquisition. To partially mitigate these differences, the subjects participating in the collection of the reference data were asked to open their eyes as widely as possible, cf. Fig. \ref{fig:samplesFromDataset}.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.45\textwidth]{PAD/sample-cold}\hskip1mm
	\includegraphics[width=0.45\textwidth]{PAD/sample-warm}
	\caption{Example iris images obtained from a dead \textbf{(left)} and a live subject \textbf{(right)}.}
	\label{fig:samplesFromDataset}
\end{figure}

To examine whether the metal retractors will serve as cues for the DCNN when it is trained to discern post-mortem irises from the live ones, we have employed the class activation mapping technique described in Section \ref{sec:CAM} with the original, uncropped images. This implementation was done using the Keras framework \cite{Keras}, employing an adapted code from \cite{GradCAMCode}. 

Example predictions were then obtained for both the \emph{live} and \emph{post-mortem} classes, Figs. \ref{fig:bias-analysis-cam-cold} and \ref{fig:bias-analysis-cam-warm}. This shows that the metal parts of the medical equipment used to open the eyelids of deceased subjects indeed provide class discriminatory cues (Fig. \ref{fig:bias-analysis-cam-cold}), which in this case is undesirable, as we want our network to recognize post-mortem irises, and not the equipment used for post-mortem data collection. Also, for samples with no metal parts visible, but with heavily distorted eyelids, the network pays attention not to the iris itself, but rather to its surrounding (Fig. \ref{fig:bias-analysis-cam-cold}). In none of these cases can we see strong activations by the iris region. However, when distinctive features such as metal retractors or heavily distorted eyelids are absent from the image, and its overall resemblance to an image of a live iris is strong, the model does focus on the iris region, but fails to make a correct prediction, Fig. \ref{fig:bias-analysis-cam-cold-failure}.

Notably, a similar behavior can be observed for images of live irises. When analyzing example activation maps for the \emph{live} class, we see that it is the iris region that produces the strongest activations, while the iris surroundings remain mostly unused, as depicted in Fig. \ref{fig:bias-analysis-cam-warm}. This behavior, contrary to what we observe with post-mortem samples, can be considered desirable.      


\begin{figure}[t!]
	\centering
		\footnotesize{\textcolor{mygreen}{\bf post-mortem presented, post-mortem detected \hskip7mm post-mortem presented, post-mortem detected}}\\
	\includegraphics[width=0.235\textwidth]{PAD/0003_L_2_1_NIR.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/0003_L_2_1_NIRbmpgradcam.jpg}\hskip5mm
	\includegraphics[width=0.235\textwidth]{PAD/0007_L_2_1_NIR.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/0007_L_2_1_NIRbmpgradcam.jpg}\hskip1mm
	\caption{Example class activation maps obtained using the Grad-CAM technique for samples from the original, unmodified dataset, with a model trained on the original dataset. Correct classification of \emph{post-mortem} is shown. Graphics adapted from \cite{TrokielewiczColdPAD_BTAS2018}.} 
	\label{fig:bias-analysis-cam-cold}
\end{figure}

\begin{figure}[t!]
	\centering
	\footnotesize{\textcolor{myred}{\bf post-mortem presented, live detected}}\\
	\includegraphics[width=0.235\textwidth]{PAD/cold-failure.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/cold-failurebmpgradcam.jpg}
	\caption{Same as in Fig. \ref{fig:bias-analysis-cam-cold}, but with \emph{post-mortem} sample misclassified as \emph{live}. Graphics adapted from \cite{TrokielewiczColdPAD_BTAS2018}.} 
	\label{fig:bias-analysis-cam-cold-failure}
\end{figure}

\begin{figure}[t!]
	\centering
	\footnotesize{\textcolor{mygreen}{\bf live presented, live detected \hskip34mm live presented, live detected}}\\
	\includegraphics[width=0.235\textwidth]{PAD/0003_L_1_1_NIR.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/0003_L_1_1_NIRbmpgradcam.jpg}\hskip5mm
	\includegraphics[width=0.235\textwidth]{PAD/0007_R_1_2_NIR.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/0007_R_1_2_NIRbmpgradcam.jpg}
	\caption{Same as in Fig. \ref{fig:bias-analysis-cam-cold}, but for \emph{live} samples. Graphics adapted from \cite{TrokielewiczColdPAD_BTAS2018}.} 
	\label{fig:bias-analysis-cam-warm}
\end{figure}

\subsection{Quantitative bias assessment}
For a quantitative evaluation of the variations between images coming from the two datasets that may originate in difference between camera operators or the environment, we have calculated three covariates related to iris quality, namely: average intensity, grayscale utilization (image histogram entropy), and image sharpness (the latter two being suggested by the ISO \cite{ISO2}):

\begin{itemize}
	\item \textbf{average image intensity}: 
	\begin{equation}
			AI=\frac{1}{N}\sum_{i=1}^{n}\sum_{j=1}^{m}I_{ij} 
	\end{equation}
	where $I_{ij}$ is the pixel intensity and $n,m$ is the image size in pixels;
	
	\item \textbf{grayscale utilization}, or the entropy of the iris image histogram $H$, measured in bits, examines pixel values of an iris image to calculate a spread of intensity values and assess whether the image is properly exposed:
	\begin{equation}
	H=-\sum_{i=1}^{256}p_ilog_2p_i	
	\end{equation}
	
	 where $p_i$ is the probability of each gray level $i$ occurring in the image, hence, the total count of pixels at gray level $i$, divided by the total number of pixels;

	\item \textbf{sharpness}, determined by the power resulting from convolving the image with a Laplacian of Gaussian kernel with $\sigma=1.4$. For convenience, we repeat this formula from the standard. Let
	 
	\begin{equation}
		F=\begin{bmatrix}
			0 & 1 & 1 & 2 & 2 & 2 & 1 & 1 & 0 \\
			1 & 2 & 4 & 5 & 5 & 5 & 4 & 2 & 1 \\
			1 & 4 & 5 & 3 & 0 & 3 & 5 & 4 & 1 \\
			2 & 5 & 3 & -12 & -24 & -12 & 3 & 5 & 2 \\
			2 & 5 & 0 & -24 & -40 & -24 & 0 & 5 & 2 \\
			2 & 5 & 3 & -12 & -24 & -12 & 3 & 5 & 2 \\
			1 & 4 & 5 & 3 & 0 & 3 & 5 & 4 & 1 \\
			1 & 2 & 4 & 5 & 5 & 5 & 4 & 2 & 1 \\
			0 & 1 & 1 & 2 & 2 & 2 & 1 & 1 & 0 	
		\end{bmatrix}
	\end{equation}
	
	be the convolutional kernel, with which the image $I(x,y)$ is convolved and a weighted sum of the filter response is computed for every fourth row and column location in $I(x,y)$ to produce filtered image $I_{F}(x,y)$: 
	
	\begin{equation}
		I{_F}(x,y) = \sum_{i=-4}^{4}\sum_{i=-4}^{4}I(x+i,y+j)F(i+5,j+5)\forall{x}\in[1,5,...,w],y\in[1,5,...,h]
	\end{equation}
	
	where $w$ and $h$ are the width and height of $I(x,y)$, respectively. Then the squared sum $SS$ of the $I_{F}(x,y)$ is computed:
	
	\begin{equation}
		SS = \sum_{\forall{x,y}\in{I{_F}(x,y)}}I{_F}(x,y)^2
	\end{equation}
	
	and the power $P$ in $I_{F}$:
	\begin{equation}
		P = \frac{SS}{w_F \times h_F}
	\end{equation}
	
	where $w_F$ and $h_F$ are the width and height of $I_{F}(x,y)$, respectively. Finally, the sharpness covariate $SH$ is given by:
	
	\begin{equation}
		SH=100 \times \frac{P^2}{P^2 + c^2}
	\end{equation}
	
	where the $c$ constant has been chosen empirically to be $1800000$.
	
	\end{itemize}

Results of these calculations are shown in Fig. \ref{fig:boxplots}. Notably, only the \emph{sharpness} covariate differs largely between the two datasets, as post-mortem iris images have lower sharpness on average. This can be a result of a combination of factors, such as: more difficult collection environment (\eg a hospital mortuary), a less experienced operator (\eg medical staff), limited time, and such. For completeness, to provide formal statistical analysis, we ran a Wilcoxon rank-sum test for each pair of covariates, which revealed that there are statistically significant differences between the subsets of live and post-mortem iris images, as the null hypothesis stating that the compared scores are samples from continuous distributions with equal median was rejected at significance level $\alpha=0.05$ in all three cases. However, all three pairs of covariates do not provide enough differentiation between the images coming from different datasets, to themselves serve as features for presentation attack detection detection.

\begin{figure*}[h!]
	\centering
	\includegraphics[width=0.32\textwidth]{PAD/boxplots-intensity-eps-converted-to}\hskip3mm
	\includegraphics[width=0.32\textwidth]{PAD/boxplots-grayscale-eps-converted-to}\hskip3mm
	\includegraphics[width=0.32\textwidth]{PAD/boxplots-sharpness-eps-converted-to}
	\caption{Boxplots representing differences in image quality metrics between the two datasets. Median values are shown in red, height of each boxes corresponds to an inter-quartile range (IQR) spanning from the first (Q1) to the third (Q3) quartile, whiskers span from Q1-1.5*IQR to Q3+1.5*IQR, and outliers are shown as crosses. Figures reprinted from \cite{TrokielewiczColdPAD_BTAS2018}.}
	\label{fig:boxplots}
\end{figure*}



\subsection{Dataset modification to counteract the bias}
To force-shift the network's attention to the iris, and not its neighborhood, we have manually segmented all images in both datasets, approximating the outer iris boundary with a circle with a radius $R_i$ and then cropping and masking the image to the size of $1.2 R_i$, see Fig. \ref{fig:samplesFromDataset_Modified}. This margin of $0.2 R_i$ is preserved purposefully, to represent the differences in the iris surroundings and in the iris-sclera boundary, and not only these present in the tissue itself. The same reasoning is behind leaving the pupillary region unmasked, as the appearance of the pupil can bear liveness information as well. The raw images obtained from the sensor are thus referred to as the \emph{original dataset}, while the modified version is called the \emph{cropped\_masked dataset}. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.235\textwidth]{PAD/sample-cold_croppedMasked}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/sample-warm_croppedMasked}
	\caption{Cropped\_masked versions of the images from Fig. \ref{fig:samplesFromDataset}.}
	\label{fig:samplesFromDataset_Modified}
\end{figure}

To validate this reasoning, we train the same model that was employed for assessing class activation maps in the unmodified data, but with the \emph{cropped\_masked} images instead. Activation maps drawn for example \emph{cropped\_masked} iris images are shown in Figs. \ref{fig:bias-analysis-cam-cold-cropped}, \ref{fig:bias-analysis-cam-cold-failure-cropped}, and \ref{fig:bias-analysis-cam-warm-cropped}. As for the \emph{post-mortem} samples, the new model now mostly focuses on the iris and its boundary, cf. Fig. \ref{fig:bias-analysis-cam-cold-cropped}, which seems reasonable, as the iris-sclera boundary is quickly getting blurry as time since death progresses. This is different for the problematic sample examined earlier in Fig. \ref{fig:bias-analysis-cam-cold-failure}, for which the activation map is centered near the pupillary region of the eye. This sample, however, is now correctly classified as a post-mortem one. 

When it comes to samples representing live eyes, the network also seems to assess their PAD score by analyzing the iris-boundary region, but to some degree it also brings its attention to the iris itself, and to the specular reflection found in the middle of the pupil, as depicted in Fig. \ref{fig:bias-analysis-cam-warm-cropped}. These features seem to offer enough discriminatory power to successfully differentiate between the post-mortem and live samples, as the validation accuracy on the \emph{cropped\_masked} subset of subject-disjoint iris images reaches 100\%, compared to less than 95\% obtained for the unmodified set of the same iris images. This also shows that we have managed to successfully shift the attention of the network towards the iris features and discriminative information they offer in recognizing cadaver samples.

\begin{figure}[t!]
	\centering
	\footnotesize{\textcolor{mygreen}{\bf post-mortem presented, post-mortem detected \hskip5mm post-mortem presented, post-mortem detected}}\\
	\includegraphics[width=0.235\textwidth]{PAD/0007_L_1_1_NIR_croppedMasked.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/0007_L_1_1_NIR_croppedMaskedbmpgradcam.jpg}\hskip5mm
	\includegraphics[width=0.235\textwidth]{PAD/0003_R_2_1_NIR_croppedMasked.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/0003_R_2_1_NIR_croppedMaskedbmpgradcam.jpg}
	\caption{Example class activation maps for samples from the \emph{cropped\_masked} dataset, with a model trained on the \emph{cropped\_masked} dataset as well. Correct classification of \emph{post-mortem} samples. Graphics adapted from \cite{TrokielewiczColdPAD_BTAS2018}.} 
	\label{fig:bias-analysis-cam-cold-cropped}
\end{figure}

\begin{figure}[t!]
	\centering
		\footnotesize{\textcolor{mygreen}{\bf post-mortem presented, post-mortem detected}}\\
	\includegraphics[width=0.235\textwidth]{PAD/cold-failure-croppedMasked.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/cold-failure-croppedMaskedbmpgradcam.jpg}
	\caption{Cropped version of the \emph{post-mortem} sample misclassified earlier in Fig. \ref{fig:bias-analysis-cam-cold-failure}. The classification is now \textbf{correct}. Graphics adapted from \cite{TrokielewiczColdPAD_BTAS2018}.} 
	\label{fig:bias-analysis-cam-cold-failure-cropped}
\end{figure}


\begin{figure}[t!]
	\centering
\footnotesize{\textcolor{mygreen}{\bf live presented, live detected \hskip34mm live presented, live detected}}\\
	\includegraphics[width=0.235\textwidth]{PAD/0007_L_1_4_NIR_croppedMasked.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/0007_L_1_4_NIR_croppedMaskedbmpgradcam.jpg}\hskip5mm
	\includegraphics[width=0.235\textwidth]{PAD/0003_R_1_2_NIR_croppedMasked.jpg}\hskip1mm
	\includegraphics[width=0.235\textwidth]{PAD/0003_R_1_2_NIR_croppedMaskedbmpgradcam.jpg}\hskip1mm
	\caption{Same as in Fig. \ref{fig:bias-analysis-cam-cold-cropped}, but for \emph{live} samples. Graphics reprinted from \cite{TrokielewiczColdPAD_BTAS2018}.} 
	\label{fig:bias-analysis-cam-warm-cropped}
\end{figure}

\section{Results and discussion}
\label{sec:results}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.48\textwidth]{PAD/accuracy-averaged-eps-converted-to}\hskip5mm
		\includegraphics[width=0.48  \textwidth]{PAD/ROC-averaged-eps-converted-to}
		\caption{{\bf (Right:)} Accuracy of classification into live and post-mortem classes, achieved in 20 independent, subject-disjoint train/test data splits. {\bf (Left:)} ROC curves showing classification accuracy averaged over 20 independent, subject-disjoint train/test data splits. AUC denotes the Area Under Curve metric. Graphs reprinted from \cite{TrokielewiczColdPAD_BTAS2018}.}
	\label{fig:accuracy_and_ROCs}
\end{figure}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.99\textwidth]{PAD/accuracy-timewise-eps-converted-to}\vskip-5mm
		\caption{Boxplots representing differences in liveness scores earned by the samples acquired in different moments after death. {\bf The lower the score, the more likely the sample represents a post-mortem eye}. Samples denoted as acquired \textbf{zero hours} post-mortem are those collected from live irises. Figures reprinted from \cite{TrokielewiczColdPAD_BTAS2018}.}
	\label{fig:accuracy-timewise}
\end{figure}

\subsection{Averaged classification accuracy}
Several metrics can be utilized to evaluate the classification accuracy of our solution. First, we average the accuracy achieved in each of the twenty train/test splits, measured as a share of correctly assigned labels to the overall number of trials in a given split, Fig. \ref{fig:accuracy_and_ROCs}. Notably, in most of the splits, the solution achieves a 100\% classification accuracy on the test subset, with the average of 98.94\%.           

\subsection{Global performance by Receiver Operating Characteristic}
Receiver Operating Characteristic curves (ROC) are often employed for visualizing performance of classification systems, especially when binary classification is in place. Thus, we present ROC graphs with Areas Under Curve (AUC) calculated for each curve in Fig. \ref{fig:accuracy_and_ROCs}, right plot. The obtained classification accuracy is almost perfect on the whole set of samples (AUC=99.94\%), and even better when samples acquired shortly (\ie five hours) after death are excluded from the dataset (AUC=99.99\%). We elaborate more on this exclusion in the following Section.  

\subsection{Classification accuracy in respect to post-mortem time horizon}

Having post-mortem samples acquired for different subjects at multiple time points after death, makes it possible to analyze the performance of our solution in respect to the time that has passed since a subject's demise, Fig. \ref{fig:accuracy-timewise}. Interestingly, and perhaps accordingly to a common sense, the probability of a sample being post-mortem increases as time since death elapses. We can expect a few false matches (post-mortem samples being classified as live samples) with images obtained 5 hours after death, regardless of the chosen threshold. This can be attributed to the fact that these images are very similar to those obtained from live individuals, as post-mortem changes to the eye are still not pronounced enough to allow for a perfect classification accuracy. However, the already good accuracy is getting close-to-perfect when these samples are not taken into consideration, Fig. \ref{fig:accuracy_and_ROCs}, red dotted line. Attack Presentation Classification Error Rate (APCER, misclassifying post-mortem samples as live ones) equal to zero can be achieved in such scenario, provided that an appropriate acceptance threshold is established. Bona Fide Presentation Classification Error Rate (BPCER, misclassifying live samples as post-mortem ones) in such case is only  $\approx$1\%. 
     
\section{Conclusions}
This Chapter introduces the first known to us method for iris liveness detection in respect to the post-mortem setting, The proposed PAD component is able to correctly classify nearly 99\% of the samples, assigning \emph{alive} or \emph{post-mortem} labels, respectively. An attempt to explain the reasoning behind the decisions provided by our DCNN-based solution is also made, especially with respect to the iris regions that are considered when making these decisions. By employing the Grad-CAM class activation mapping technique, we managed to show that the image regions bearing the most useful discriminatory cues for liveness detection are those containing iris-sclera boundary, and to some extent also the pupillary region.                 
        
{\bf This proves the last, fourth thesis formulated in Sec. \ref{sec:theses}, namely that liveness detection method can be proposed to discern post-mortem iris samples from those collected from living individuals, requiring only a static iris image.}

