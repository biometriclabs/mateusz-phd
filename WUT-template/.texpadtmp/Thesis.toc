\select@language {english}
\contentsline {chapter}{List of Abbreviations}{4}
\contentsline {chapter}{\numberline {1\relax .\kern .5em }Post-mortem iris recognition}{6}
\contentsline {section}{\numberline {1.1\relax .\kern .5em }Iris recognition after death}{6}
\contentsline {section}{\numberline {1.2\relax .\kern .5em }Related work}{7}
\contentsline {section}{\numberline {1.3\relax .\kern .5em }Dataset of cadaver iris images}{9}
\contentsline {subsection}{\numberline {1.3.1\relax .\kern .5em }Data Collection}{9}
\contentsline {subsection}{\numberline {1.3.2\relax .\kern .5em }Statistics}{11}
\contentsline {section}{\numberline {1.4\relax .\kern .5em }Medical background}{11}
\contentsline {section}{\numberline {1.5\relax .\kern .5em }Quantification of influence on iris recognition}{13}
\contentsline {section}{\numberline {1.6\relax .\kern .5em }Results and discussion}{13}
\contentsline {chapter}{References}{14}
\contentsline {chapter}{Appendix A: Terms and definitions used}{16}
\contentsline {section}{Harmonized Biometric Vocabulary}{16}
\contentsline {section}{ISO/IEC standard for biometric testing}{16}
\contentsline {chapter}{Appendix B: \\Author's publications, talks, and awards}{17}
\contentsline {section}{Journal publications}{17}
\contentsline {section}{Conference publications}{17}
\contentsline {section}{Poster conference presentations}{19}
\contentsline {section}{Conference talks}{19}
\contentsline {section}{Awards and achievements}{19}
