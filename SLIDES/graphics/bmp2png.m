clear all
close all
clc

% iterate through pics
datasetFolder = './';
targetFolder = './';
imageList = dir([datasetFolder '*.bmp']);

% FIRST LOOP: ITERATE THROUGH IMAGES
for i = 1:length(imageList)
            
    imageName = imageList(i).name;
    imagePath = [datasetFolder imageList(i).name]
    
    % image instance for getting features
    image = imread(imagePath);
    
    imwrite(image, [imagePath(1:end-4) '.png']);
    
end