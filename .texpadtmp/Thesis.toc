\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{}
\contentsline {section}{\numberline {1.1}Iris recognition as a biometric utility}{}
\contentsline {section}{\numberline {1.2}New challenges in iris biometrics research}{}
\contentsline {section}{\numberline {1.3}Aim of this Thesis}{}
\contentsline {section}{\numberline {1.4}Proposed methodology}{}
\contentsline {subsection}{\numberline {1.4.1}Quantification of biology and pathology induced changes influence}{}
\contentsline {subsection}{\numberline {1.4.2}New methods in iris image processing}{}
\contentsline {section}{\numberline {1.5}Terms and definitions used}{}
\contentsline {subsection}{\numberline {1.5.1}Harmonized Biometric Vocabulary}{}
\contentsline {subsection}{\numberline {1.5.2}ISO/IEC standard for biometric testing}{}
\contentsline {chapter}{\numberline {2}Aging influence}{}
\contentsline {section}{\numberline {2.1}Iris template non-stationarity}{}
\contentsline {section}{\numberline {2.2}Related work}{}
\contentsline {chapter}{\numberline {3}Ophthalmic disorders influence}{}
\contentsline {section}{\numberline {3.1}Iris recognition in the presence of ocular pathologies}{}
\contentsline {section}{\numberline {3.2}Related work}{}
\contentsline {section}{\numberline {3.3}Experiments and results}{}
\contentsline {section}{\numberline {3.4}Discussion}{}
\contentsline {chapter}{\numberline {4}Post-mortem iris recognition}{}
\contentsline {section}{\numberline {4.1}Iris recognition in post-mortem setting}{}
\contentsline {section}{\numberline {4.2}Related work}{}
\contentsline {section}{\numberline {4.3}Experiments and results}{}
\contentsline {section}{\numberline {4.4}Discussion}{}
\contentsline {chapter}{\numberline {5}Iris recognition under biologically troublesome conditions}{}
\contentsline {section}{\numberline {5.1}Image segmentation}{}
\contentsline {subsection}{\numberline {5.1.1}Related work}{}
\contentsline {subsubsection}{Convolutional networks for semantic segmentation}{}
\contentsline {subsubsection}{Neural networks for iris image segmentation}{}
\contentsline {section}{\numberline {5.2}Feature extraction}{}
\contentsline {subsection}{\numberline {5.2.1}Related work}{}
\contentsline {subsubsection}{Iris crypts for recognition}{}
\contentsline {subsubsection}{Other macro features of the iris}{}
\contentsline {subsubsection}{Filtering-based methods (but not Daugman)}{}
\contentsline {subsubsection}{Morphological structures of the iris}{}
\contentsline {subsubsection}{Methods employing neural networks}{}
\contentsline {subsubsection}{SIFT-based approaches}{}
\contentsline {section}{\numberline {5.3}Performance evaluation}{}
\contentsline {subsection}{\numberline {5.3.1}Test databases}{}
\contentsline {chapter}{\numberline {6}Conclusions and way forward}{}
\contentsline {chapter}{\numberline {A}List of author's publications and achievements}{}
\contentsline {subsubsection}{JOURNAL PAPERS}{}
\contentsline {subsubsection}{CONFERENCE PROCEEDINGS}{}
\contentsline {subsubsection}{CONFERENCE TALKS}{}
\contentsline {subsubsection}{POSTER PRESENTATIONS}{}
\contentsline {subsubsection}{AWARDS AND ACHIEVEMENTS}{}
